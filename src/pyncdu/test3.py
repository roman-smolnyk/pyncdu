import os


def get_dir_size(root):
    size = 0
    previous_path = ""
    for path, dirs, files in os.walk(root):
        if previous_path == path:
            print("STUCK")
        else:
            previous_path = path
        for f in files:
            if os.path.islink(f):
                continue
            try:
                size += os.path.getsize(os.path.join(path, f))
            except Exception as e:
                pass
    return size


print(get_dir_size("C:\\"))
