import win32com.client
import pywintypes

def get_folder_size(target_folder):
    fso = win32com.client.Dispatch("Scripting.FileSystemObject")
    fobj = fso.GetFolder(target_folder)
    return fobj.size


if __name__ == '__main__':
    try:
        print(get_folder_size('C:\\Windows.old'))
    except Exception as e:
        print(e)  # debug, have to see which indices
        # print(hex(e[0]+2**32), hex(e[2][5]+2**32))